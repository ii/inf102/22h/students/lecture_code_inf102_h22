package lecture5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InsertionSorter implements Sorter {

	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) { //O(n^2)

		ArrayList<T> sorted = new ArrayList<>(list.size());
		for(T t : list) { //n iterations
			int index = Collections.binarySearch(sorted, t); //O(log n)
			if(index<0) {
				index = -index-1;
			}
				
			sorted.add(index,t); //O(n)
		}
		for(int i=0; i<sorted.size(); i++) { //n iterasjoner
			list.set(i, sorted.get(i)); //O(1)
		}
	}

}

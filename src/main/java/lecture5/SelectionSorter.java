package lecture5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SelectionSorter implements Sorter { //O(n^2)

	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) {
		List<T> sorted = new ArrayList<T>();
		while(!list.isEmpty()) { //n iterasjoner
			T smallest = Collections.min(list); //O(n)
			list.remove(smallest); //O(n)
			sorted.add(smallest); //O(1)
		}
		list.addAll(sorted); //O(n)
	}

}

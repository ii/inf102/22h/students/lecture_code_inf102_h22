package eksamenV20;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lecture1.StringListGenerator;
import lecture5.Sorter;

public class MergeSort2 implements Sorter{

	
	public static void main(String[] args) {
		MergeSort2 ms = new MergeSort2();
		List<String> list = StringListGenerator.generateStringList(10);
		ms.sort(list);
		System.out.println(list);
	}
	
	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) {
		sort(list,0,list.size()-1);
	}

	public <T extends Comparable<? super T>> void sort(List<T> list, int from, int to) {
		if(from>=to) //No need to sort if only 1 element
			return;
		int mid = (from+to)/2;
		sort(list,from,mid);
		sort(list,mid+1,to);
		if(list.get(mid).compareTo(list.get(mid+1))<=0)//already sorted
			return;
		else {
			List<T> merged = merge(list.subList(from, mid+1),list.subList(mid+1, to+1));
			Collections.copy(list.subList(from, to+1), merged);
		}
	}

	private <T extends Comparable<? super T>> List<T> merge(List<T> list1, List<T> list2) {
		int n = list1.size()+ list2.size();
		List<T> list = new ArrayList<T>(n);
		//Merge
		int i1=0;
		int i2=0;
		while(i1<list1.size() && i2<list2.size()) {//n iterations
			T t1 = list1.get(i1);
			T t2 = list2.get(i2);
			if(t1.compareTo(t2)<=0) {
				list.add(t1);
				i1++;
			}
			else {
				list.add(t2);
				i2++;
			}
		}
		while(i1<list1.size())
			list.add(list1.get(i1++));
		while(i2<list2.size())
			list.add(list2.get(i2++));
		return list;
	}
	
	
}

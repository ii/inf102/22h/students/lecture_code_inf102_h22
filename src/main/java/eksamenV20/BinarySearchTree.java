package eksamenV20;

import java.util.ArrayList;
import java.util.List;

public class BinarySearchTree {

	public static void main(String[] args) {
		Node<String> parent = null; //here we need code to genenerate an sample binary search tree
		int k=10;
		kSmall(parent, k);
	}

	private static <Key> Key kSmall(Node<Key> parent, int k) {
		List<Key> smallest = new ArrayList<>();
		kSmall(parent, k, smallest);
		if(smallest.size()>=k)
			return smallest.get(k-1);
		else
			return null;
	}

	/**
	 * This method will search through a subtree and fill up
	 * an array with the k smallest elements in a binary search tree
	 * This is a DFS where we add nodes to the list after recursive call to left child
	 * but before call to right child.
	 * 
	 * @param <Key> Type of elements in the Binary search tree
	 * @param parent - The start node
	 * @param k - the final size of the array to be filled
	 * @param smallest - The array to fill
	 */
	static <Key> void kSmall(Node<Key> parent, int k, List<Key> smallest) {
		//We fill the smallest elements first, if there are any 
		//smaller elements than parent they will in the left subtree
		if(parent.left != null) {
			kSmall(parent.left, k, smallest);
		}
		//if left subtree contained less than k elements then parent must be in list
		if(smallest.size()<k) {
			smallest.add(parent.key);
		}
		if(smallest.size()>=k) {
			//if we already found all elements we need we are done.
			return;
		}
		//if we still have not found k elements we need to select some elements from Right subtree
		if(parent.right!=null)
			kSmall(parent.right, k, smallest);
	}
}

class Node<K> {
	K key;
	Node<K> left, right;
}
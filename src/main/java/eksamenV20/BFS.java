package eksamenV20;

import java.util.HashMap;
import java.util.LinkedList;

import lecture11.Edge;
import lecture11.Graph;
import lecture11.IGraph;

public class BFS {

	public static void main(String[] args) {
	
		//Note here we need to divide by 2 since every edge is counted twice
		int count = countEqualEdgesBFS(makeSampleGraph(), 0)/2;
		System.out.println(count);

	}
	/**
	 *   4-1-0
	 *   |\| |
	 *   3-2-5
	 *   |/ 
	 *   6 
	 */
	static Graph<Integer> makeSampleGraph() {
		Graph<Integer> g = new Graph<Integer>();
		g.addVertex(0);
		g.addVertex(1);
		g.addVertex(2);
		g.addVertex(3);
		g.addVertex(4);
		g.addVertex(5);
		g.addVertex(6);
		g.addEdge(0, 1);
		g.addEdge(0, 5);
		g.addEdge(1, 2);		
		g.addEdge(1, 4);		
		g.addEdge(2, 4);
		g.addEdge(2, 5);
		g.addEdge(2, 6);
		g.addEdge(3, 4);
		g.addEdge(3, 6);
		return g;
	}
	
	public static <V> int countEqualEdgesBFS(IGraph<V> g, V start) { //O(m)
		HashMap<V,Integer> distance = new HashMap<>(); //O(1)
		LinkedList<Edge<V>> toSearch = new LinkedList<Edge<V>>(); //O(1)
		distance.put(start,0); //O(1)
		addNeighbourEdges(start,g,toSearch); //O(n)
		int count=0; //O(1)
		
		while(!toSearch.isEmpty()) { //O(m) iterasjoner
			Edge<V> next = toSearch.removeLast(); //O(1) //Change to RemoveFirst to get DFS
			if(distance.containsKey(next.b)) { //O(1) forventet
				if(distance.get(next.a)==distance.get(next.b))
					count++;
				continue; //edge between 2 green
			}
			//O(n) iterations past here once for each node
			int d = distance.get(next.a)+1;
			distance.put(next.b,d); //O(1) forventet
			System.out.println("Visiting "+next);
			addNeighbourEdges(next.b, g, toSearch); //O(degree(next))
		}
		return count;
		
	}

	private static <V> void addNeighbourEdges(V start, IGraph<V> g, LinkedList<Edge<V>> toSearch) {
		for(V neighbour : g.neighbours(start)) {
			toSearch.addFirst(new Edge(start,neighbour));
		}
	}

}

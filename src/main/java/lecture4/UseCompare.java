package lecture4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class UseCompare {

	public static void main(String[] args) {
		int n=1000;
		Random rand = new Random();
		
		ArrayList<Integer> tall = new ArrayList<>();
		for(int i=0;i<n; i++) {
			tall.add(rand.nextInt(2*n));
		}
		
		printFirst10(tall);
		
		Collections.sort(tall);

		printFirst10(tall);
		
		int key = 102;
		int index = Collections.binarySearch(tall, key);
		if(index>=0) {
			System.out.println(key + " = " + tall.get(index));
		}else {
			index = -(index+1);
			if(index>0)
				System.out.println(key +" >= "+tall.get(index-1));
			if(index<n)
				System.out.println(key +" <= "+tall.get(index));
		}
		
		Comparator<Integer> comp = new TverrsumComparator();
		Collections.sort(tall, comp);
		
		printFirst10(tall);
}

	private static void printFirst10(ArrayList<Integer> tall) {
		for(int i=0; i<10; i++) {
			System.out.println(tall.get(i));
		}
		System.out.println("...");
	}
	
}

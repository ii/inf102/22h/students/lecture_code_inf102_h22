package lecture4;

import java.util.HashMap;

public class Fibonacci {

	static HashMap<Integer,Long> memory = new HashMap<>();
	
	public static void main(String[] args) {
		for(int i=1; i<2000; i++)
			System.out.println(fib(i));
	}
	
	//n must be integer >= 1
	public static long fib(int n){
		if(n<=2) {
			return n-1;
		}
		
		if(memory.containsKey(n)) {
			return memory.get(n);
		}
		long ans = fib(n-1)+fib(n-2);
		memory.put(n, ans);
		return ans;
	}
}

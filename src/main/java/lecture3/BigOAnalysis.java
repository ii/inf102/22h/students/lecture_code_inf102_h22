package lecture3;

import java.util.ArrayList;

public class BigOAnalysis {

	public static void main(String[] args) {
		//order1(12,5);
		//loopDeLoop(10);
		//whatIf(17);
		recursivePrint(10);
	}
	//n er lengden av numbers
	public static void useList(ArrayList<Integer> numbers) { //O(n)
		int a = numbers.get(0); //O(1)
		int b = numbers.get(1); //O(1)
		int sum = a+b; //O(1)
		numbers.add(0,sum); //O(n)
	}
	
	public static void order1(int a, int b) {
		int sum = a + b; //O(1)
		System.out.println("Summen av "+a+" + "+b+" er "+sum); //O(1)
	}
	
	public static void loopDeLoop(int n) {
		for(int i=0; i<n; i++) { //n iterasjoner totalt O(n) tid
			System.out.println("Loop de loop, flip flop.");//O(1) 
			System.out.println("Flying in an aeroplane."); //O(1)
		}
	}

	public static void whatIf(int n) {//O(n)
		if(n%2==0) {
			loopDeLoop(n); //O(n)
		}
		else {
			order1(n, n);  //O(1)
		}		
	}

	public static void whatIf2(int n) { //O(1)
		if(n<=10) { //O(1)
			loopDeLoop(n); //O(n)
		}
		else { //O(1)
			order1(n, n); //O(1)
		}		
	}

	public static void recursivePrint(int n) { //O(n)
		if(n==1) {
			System.out.print(1); //1 gang O(1)
		}
		else {
			System.out.print(n+" "); //n ganger O(1) O(n)
			recursivePrint(n-1);//n ganger O(1) O(n)
		}
	}

	public static String StringOperation(int n) {
		String tekst = "Hello"; //O(1)
		char[] tekstArray = tekst.toCharArray(); //O(1)
		
		StringBuffer sb = new StringBuffer("Hello"); //O(1)
		ArrayList<String> sbArray = new ArrayList<String>(); //O(1)
		
		for(int i=0; i<n; i++) { //O(n^2)
			tekst = tekst+"!";
		}

		for(int i=0; i<n; i++) { //O(n)
			sb.append('!');
		}

		return tekst+"!";
	}

}

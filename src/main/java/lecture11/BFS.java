package lecture11;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BFS {

	public static void main(String[] args) {
	
		String far = farthestBFS(makeSampleGraph(), "a");
		System.out.println(far);

	}
	/**
	 *   f - e - g
	 *  / | 
	 * a - b 
	 * | \ | 
	 * c - d
	 */
	static Graph<String> makeSampleGraph() {
		Graph<String> g = new Graph<String>();
		g.addVertex("a");
		g.addVertex("b");
		g.addVertex("c");
		g.addVertex("d");
		g.addVertex("e");
		g.addVertex("f");
		g.addVertex("g");
		g.addEdge("a", "b");
		g.addEdge("a", "c");
		g.addEdge("a", "d");		
		g.addEdge("a", "f");		
		g.addEdge("b", "d");
		g.addEdge("b", "f");
		g.addEdge("c", "d");
		g.addEdge("e", "g");
		g.addEdge("e", "f");
		return g;
	}
	
	public static <V> V farthestBFS(IGraph<V> g, V start) { //O(m)
		HashSet<V> found = new HashSet<>(); //O(1)
		LinkedList<V> toSearch = new LinkedList<V>(); //O(1)
		found.add(start); //O(1)
		addNeighbours(start,g,toSearch); //O(n)
		V furthest = start; //O(1)
		
		while(!toSearch.isEmpty()) { //O(m) iterasjoner
			V next = toSearch.removeLast(); //O(1) //Change to RemoveFirst to get DFS
			if(found.contains(next)) //O(1) forventet
				continue; //edge between 2 green
			//O(n) iterations past here once for each node
			found.add(next); //O(1) forventet
			System.out.println("Visiting "+next);
			furthest = next;
			addNeighbours(next, g, toSearch); //O(degree(next))
		}
		return furthest;
		
	}

	private static <V> void addNeighbours(V start, IGraph<V> g, LinkedList<V> toSearch) {
		for(V neighbour : g.neighbours(start)) {
			toSearch.addFirst(neighbour);
		}
	}
}

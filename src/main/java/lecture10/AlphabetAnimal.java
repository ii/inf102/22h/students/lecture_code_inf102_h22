package lecture10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class AlphabetAnimal {

	public static void main(String[] args) {
		//read the input
		Scanner sc = new Scanner(System.in);
		String previous = sc.next();
		int n = sc.nextInt();
		ArrayList<String> animals = new ArrayList<>(n);
		for(int i=0; i<n; i++) {
			animals.add(sc.next());
		}
		
		//add to map
		HashMap<Character,ArrayList<String>> map = new HashMap<>();
		for(char c = 'a'; c<='z'; c++) {
			map.put(c, new ArrayList<String>());
		}
		for(String animal : animals) {
			map.get(animal.charAt(0)).add(animal);
		}

		//get all animals you can say
		char start = previous.charAt(previous.length()-1);
		ArrayList<String> choices = map.get(start);
		//we will either have a win answer where next player has no move
		//a good answer where we find an answer
		//or no answer. No solution is indicated by null value
		String good = null;
		String win = null;
		for(String choice : choices) {
			if(good == null) //this happens only first iteration
				good = choice;
			if(isWinning(choice, map)) {
				win = choice;
				break; //once finding a winning answer we do not need to search further
			}
		}
		//print out answer
		if(win!=null)
			System.out.println(win+"!");
		else {
			if(good!=null)
				System.out.println(good);
			else
				System.out.println("?");
		}

	}

	//checks if a choice of animal prevents next player from finding a valid choice
	private static boolean isWinning(String choice, HashMap<Character, ArrayList<String>> map) {
		ArrayList<String> next = map.get(choice.charAt(choice.length()-1));
		for(String s : next) {
			if(!s.equals(choice))
				return false;
		}
		return true;
	}

}

package eksamenV21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;

public class CentralFinder implements ICentralFinder{

	ArrayList<Integer> list = new ArrayList<>();
	@Override
	public void add(int number) {
		int index = Collections.binarySearch(list, number);
		if(index<0)
			index = -index-1;
		list.add(index,number);
	}

	@Override
	public int removeCentralValue() {
		if(size()==0)
			throw new NoSuchElementException("No central value in empty list");
		
		//Collections.sort(list);
		return list.remove(size()/2);
	}

	@Override
	public int size() {
		return list.size();
	}


}
package eksamenV21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

public class CentralFinder2 implements ICentralFinder{

	PriorityQueue<Integer> small = new PriorityQueue<>(Comparator.reverseOrder());
	PriorityQueue<Integer> large = new PriorityQueue<>();
	
	@Override
	public void add(int number) {
		if(small.isEmpty() || number < small.peek())
			small.add(number);
		else
			large.add(number);
		balance();
	}

	private void balance() {
		while(small.size()>large.size())
			large.add(small.remove());
		while(large.size()>small.size())
			small.add(large.remove());
	}

	@Override
	public int removeCentralValue() {
		if(size()==0)
			throw new NoSuchElementException("No central value in empty list");
		balance();
		return small.remove();
	}

	@Override
	public int size() {
		return small.size()+large.size();
	}


}
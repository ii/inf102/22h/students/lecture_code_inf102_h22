package lecture2;

import java.util.List;

import lecture1.StringListGenerator;

public class TimeListReverser {

	public static void main(String[] args) {
		List<String> list = StringListGenerator.generateStringList(100000);

		//TODO: add implementations here and call timeReverser()
		ListReverser arrayImpl = new ArrayListReverser();
		ListReverser linkedImpl = new LinkedListReverser();
		
		timeReverser(list, linkedImpl);
		timeReverser(list, arrayImpl);
	
	}
	
	public static <T> List<T> timeReverser(List<T> input, ListReverser implementation) {
		long start = System.currentTimeMillis();
		List<T> reversed = implementation.reverse(input);
		long end = System.currentTimeMillis();
		double time = (end-start)/1000.0;
		System.out.println(implementation.getClass().getName()+" reversed a list with "+input.size()+" elements in "+time+"seconds.");
		return reversed;
	}

}

package lecture12;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

import lecture11.Graph;
import lecture11.IGraph;

public class Dijkstra {

	public static void main(String[] args) {
	
		Map<String, Integer> shortest = shortestPath(makeSampleGraph(), "a");
		for(String v : shortest.keySet())
			System.out.println("Path from a to "+v+": "+shortest.get(v));

	}
	/**
	 *   f - e - g
	 *  / | 
	 * a - b 
	 * | \ | 
	 * c - d
	 */
	static WeightedGraph<String,Integer> makeSampleGraph() {
		WeightedGraph<String,Integer> g = new WeightedGraph<String,Integer>();
		g.addVertex("a");
		g.addVertex("b");
		g.addVertex("c");
		g.addVertex("d");
		g.addVertex("e");
		g.addVertex("f");
		g.addVertex("g");
		g.addEdge("a", "b",5);
		g.addEdge("a", "c",7);
		g.addEdge("a", "d",12);		
		g.addEdge("a", "f",3);		
		g.addEdge("b", "d",9);
		g.addEdge("b", "f",5);
		g.addEdge("c", "d",4);
		g.addEdge("e", "g",3);
		g.addEdge("e", "f",11);
		return g;
	}
	
	public static <V> Map<V,Integer> shortestPath(WeightedGraph<V,Integer> g, V start) { //O(m)
		HashSet<V> found = new HashSet<>(); //O(1)
		PriorityQueue<Pair<V>> toSearch = new PriorityQueue<Pair<V>>(); //O(1)
		HashMap<V,Integer> distances = new HashMap<>();
		found.add(start); //O(1)
		distances.put(start, 0);
		
		addNeighbours(new Pair<V>(start,0),g,toSearch); //O(n)
		
		while(!toSearch.isEmpty()) { 
			Pair<V> next = toSearch.remove(); 
			if(found.contains(next.node)) 
				continue; //edge between 2 green
			found.add(next.node);
			distances.put(next.node, next.weight);

			System.out.println("Visiting "+next);
			addNeighbours(next, g, toSearch); //O(degree(next))
		}
		return distances;
		
	}

	private static <V> void addNeighbours(Pair<V> start, WeightedGraph<V,Integer> g, PriorityQueue<Pair<V>> toSearch) {
		for(V neighbour : g.neighbours(start.node)) {
			int weight = start.weight + g.getWeight(start.node,neighbour);
			toSearch.add(new Pair<V>(neighbour,weight));
		}
	}
}

class Pair<V> implements Comparable<Pair<V>>{
	V node;
	Integer weight;

	public Pair(V a, Integer b) {
		this.node = a;
		this.weight = b;
	}
	
	@Override
	public int compareTo(Pair<V> o) {
		return Integer.compare(weight, o.weight);
	}
}

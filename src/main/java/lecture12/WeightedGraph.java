package lecture12;

import java.util.HashMap;
import java.util.HashSet;

import lecture11.Edge;
import lecture11.Graph;
import lecture11.IGraph;

public class WeightedGraph<V,E> implements IGraph<V>  {

	private Graph<V> g;
	private HashMap<Edge<V>,E> weights;
	
	public WeightedGraph() {
		g = new Graph<>();
		weights = new HashMap<>();
	}
	
	E getWeight(V u, V v){
		return weights.get(new Edge<V>(u,v));
	}

	@Override
	public Iterable<V> vertices() {
		return g.vertices();
	}

	@Override
	public Iterable<Edge<V>> edges() {
		return g.edges();
	}

	@Override
	public boolean adjacent(V a, V b) {
		return g.adjacent(a, b);
	}

	@Override
	public Iterable<V> neighbours(V v) {
		return g.neighbours(v);
	}

	@Override
	public int degree(V v) {
		return g.degree(v);
	}
	
	/**
	 * Adds a vertex to the graph if not already in the graph
	 * @param v the vertex to add
	 * @return true if a vertex was added, false otherwise
	 */
	public boolean addVertex(V v) {
		return g.addVertex(v);
	}
	
	/**
	 * This method connects two vertices by an edge
	 * Makes sure that both a is a neighbour of b and that b is an neighbour of a
	 * @param a,b the vertices to connect
	 * @return  true if an edge was added, false otherwise
	 */
	public boolean addEdge(V a, V b, E weight) {
		boolean change = g.addEdge(a,b);
		
		if(change) {
			weights.put(new Edge<V>(a,b), weight);
			weights.put(new Edge<V>(b,a), weight);
		}
		return change;
	}
}
